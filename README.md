# sysinfo

<div align="center">

<a href="">[![CC BY-SA 4.0][cc-by-sa-shield]][cc-by-sa]</a>
<a href="">![project status][status-shield]</a>

</div>

This work and included projects are licensed under a
[Creative Commons Attribution-ShareAlike 4.0 International License][cc-by-sa].

In addition you agree to the attached [disclaimer][disclaimer].

[cc-by-sa]: LICENSE
[cc-by-sa-shield]: https://img.shields.io/badge/license-CC%20BY--SA%204.0-informational.svg
[disclaimer]: DISCLAIMER
[status-shield]: https://img.shields.io/badge/status-active%20development-brightgreen.svg
